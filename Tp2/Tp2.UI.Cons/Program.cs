﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Tp2.UI.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            
            

            #region Exo1
            for (int index = 0; index < 10; index ++)
            {
                if (index ==5 )break;
                Console.WriteLine(index);
            }

            Console.WriteLine();
            Console.ReadKey();

            for(int v_index = 0; v_index < 10; v_index ++)
            {
                if (v_index < 7) continue;
                Console.WriteLine(v_index);
            }
            Console.ReadKey();
            #endregion
            #region Exo2
            string v_phrase;
            do
            {
                Console.WriteLine("Please write somethings");
                v_phrase = Console.ReadLine();
            } while (!(v_phrase.StartsWith("a")));

            Console.ReadKey();
            #endregion
            #region Exo3
            string[] v_semaine = { "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche" };
            foreach (string v_jour in v_semaine)
                Console.WriteLine(v_jour);
            Console.ReadKey();
            #endregion
            #region Exo4
            int v_somme;
            do
            {
                try
                {
                    v_somme = 0;
                    int[] v_buffer;
                    Console.WriteLine("Please enter a number");
                    char[] v_saisie = Console.ReadLine().ToCharArray();
                    v_buffer = new int[v_saisie.Length];
                    for (int i_index = 0; i_index < v_saisie.Length; i_index++)
                    {
                        v_buffer[i_index] = int.Parse(Convert.ToString(v_saisie[i_index]));
                        v_somme += v_buffer[i_index];
                    }
                    Console.WriteLine(v_somme);
                }
                catch (Exception)
                {
                    v_somme = 0;
                    Console.WriteLine("Please enter a real number , no text or letter");
                }

            } while (v_somme != 50);

            Console.ReadKey();
            #endregion 
            #region Exo6
            int val1 = 2, val2 = 3;
            Console.WriteLine(val1);
            Console.WriteLine(val2);
            Tools.swap<int>(ref val1,ref val2);
            Console.WriteLine(val1);
            Console.WriteLine(val2);
            Console.ReadKey();

            #endregion
        }
    }
}
