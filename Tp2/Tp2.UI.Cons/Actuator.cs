﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2.UI.Cons
{
    //Ici on attends des objets implémentants l'interface IOperation
    public class Actuator<T> where T : IOperation
    {
        private T m_device;
        public Actuator(T p_device)
        {
            m_device = p_device;
        }
        public bool Start()
        {
            bool v_return = false;
            if(!m_device.IsRunning)
            {
                m_device.Allumer();
                v_return = true;
            }
            return v_return;
        }
        public bool Stop()
        {
            bool v_return = true;
            if(m_device.IsRunning)
            {
                m_device.Eteindre();
                v_return = false;
            }
            return v_return;
        }
    }
}
