﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2.UI.Cons
{
    interface IAppareil_Electrique : IOperation
    {
        uint Consommation { get; set; }
    }
}
