﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2.UI.Cons
{
    public class Lampe : IAppareil_Electrique
    {
        private bool m_isRunning;
        private uint m_consommation;
        public uint Consommation { get => m_consommation;
            set
            {
                m_consommation = value;
                //Si Commation > 0 , alors la lampe est allumée, sinon la lampe est eteinte
                m_isRunning = Consommation > 0;
            }                   
        }

        public bool IsRunning => m_isRunning;

        public Lampe()
        {
            this.m_isRunning = false;
        }
            

        public void Allumer()
        {
            this.m_isRunning = true;
        }

        public void Eteindre()
        {
            this.m_isRunning = false;
        }
    }
}
