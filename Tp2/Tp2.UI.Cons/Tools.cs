﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2.UI.Cons
{
    class Tools
    {
        //T représente tout objet. On ne doit cependant passer que des objets identiques
        // On ne peut pas swapper un int et un string
        public static void swap<T>(ref T p_obj_1, ref T p_obj_2)
        {
            T m_buffer;
            m_buffer = p_obj_1;
            p_obj_1 = p_obj_2;
            p_obj_2 = m_buffer;
        }
    }
}
