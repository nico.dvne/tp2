﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tp2.UI.Cons
{
    public interface IOperation
    {
        void Allumer();
        void Eteindre();
        bool IsRunning { get; }
    }
}
